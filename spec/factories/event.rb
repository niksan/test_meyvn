FactoryBot.define do

  factory :event do
    name 'event name'
    start_at Date.new(2018, 1, 1)
    end_at Date.new(2018, 1, 5)
    city
  end

end
