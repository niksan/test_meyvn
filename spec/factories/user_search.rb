FactoryBot.define do

  factory :user_search do
    user
    city
    start_from Date.new(2007, 1, 1)
    start_upto Date.new(3018, 1, 1)
    topic
  end

end
