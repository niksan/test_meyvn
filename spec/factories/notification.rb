FactoryBot.define do

  factory :notification do
    user
    body 'New interested event.'
    event
  end

end
