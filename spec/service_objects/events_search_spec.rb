require 'rails_helper'

RSpec.describe EventsSearch do
  let(:user) { create :user }
  let(:topic) { create :topic }
  let(:city_vrn) { create :city, name: 'Voronezh' }
  let!(:event1) { create :event, city: city_vrn, start_at: Date.new(2018, 1, 1) }
  let(:empty_parameters) { {} }

  context '#new' do
    it 'set default values' do
      events_search = EventsSearch.new({})
      expected_params = {
        city_id: EventsSearch::DEFAULT_PARAMS[:city_id],
        topic_id: EventsSearch::DEFAULT_PARAMS[:topic_id],
        start_from: EventsSearch::DEFAULT_PARAMS[:start_from].ago.beginning_of_day,
        start_upto: EventsSearch::DEFAULT_PARAMS[:start_upto].since.end_of_day
      }
      expect(events_search.search_params).to eq(expected_params)
    end
  end

  context '#search' do
    it 'don not find events' do
      params = {
        search: {
          city_id: 9,
          topic_id: 7,
          start_from: Date.new(3019, 1, 1),
          start_upto: Date.new(3019, 1, 1)
        }
      }
      search_res = EventsSearch.new(params).search
      expect(search_res.size).to eq(0)
    end

    it 'find some events' do
      params = {
        search: {
          city_id: nil,
          topic_id: nil,
          start_from: Date.new(2017, 1, 1),
          start_upto: Date.new(2019, 1, 1)
        }
      }
      search_res = EventsSearch.new(params).search
      expect(search_res.size).to be >= (1)
    end
  end

end
