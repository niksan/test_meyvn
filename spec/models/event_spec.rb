require 'rails_helper'

RSpec.describe Event, type: :model do
  let(:user) { create :user }
  let(:topic) { create :topic }
  let(:city_vrn) { create :city, name: 'Voronezh' }
  let!(:event1) { create :event, city: city_vrn, start_at: Date.new(2018, 1, 1) }
  let!(:user_search) { create :user_search, user: user, start_from: Date.new(2018, 1, 1), start_upto:  Date.new(2019, 1, 1), city: city_vrn, topic: topic }

  context '.main_search' do

    it 'must be more than one result' do
      search_res =  Event.main_search(nil, nil, Date.new(2017, 1, 1), Date.new(2019, 1, 2))
      expect(search_res.size).to be >= (1)
    end

    it 'must be no events' do
      search_res =  Event.main_search(nil, nil, Date.new(1017, 1, 1), Date.new(1019, 1, 2))
      expect(search_res.size).to eq(0)
    end

  end

  context '.make_notifications callback' do

    it 'don t create notification' do
	    new_event = Event.create(name: '1', start_at: Date.new(3000, 7, 7), end_at: Date.new(3001, 8, 1), city: city_vrn)
      new_event.topics << topic
      count = Notification.where(event_id: new_event.id).count
      expect(count).to eq(0)
    end

    it 'don t create notification' do
	    new_event = Event.create(name: '1', start_at: Date.new(2018, 7, 7), end_at: Date.new(3001, 8, 1), city: city_vrn)
      new_event.topics << topic
      count = Notification.where(event_id: new_event.id).count
      expect(count).to be >= (1)
    end

  end

end
