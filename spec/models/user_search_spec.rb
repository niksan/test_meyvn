require 'rails_helper'

RSpec.describe UserSearch, type: :model do
  let(:user) { create :user }
  let(:topic) { create :topic }
  let(:city_vrn) { create :city, name: 'Voronezh' }
  let!(:user_search) { create :user_search, user: user, start_from: Date.new(2018, 1, 1), start_upto:  Date.new(2019, 1, 1), city: city_vrn, topic: topic }

  context '.find_by_params' do

    it 'must be more than one result' do
      search_res = UserSearch.find_by_params(
        Date.new(2018, 5, 1),
        city_vrn.id,
        [topic.id]
      )
      expect(search_res.count).to be >=(1)
    end

    it 'must be more than one result' do
      search_res = UserSearch.find_by_params(
        Date.new(3018, 5, 1),
        city_vrn.id,
        [topic.id]
      )
      expect(search_res.count).to eq(0)
    end

  end
end
