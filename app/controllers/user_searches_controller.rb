class UserSearchesController < ApplicationController
  before_action :authenticate_user!
  
  def create
    new_params = search_params.merge({ user_id: current_user.id })
    UserSearch.create(new_params)
  end

  private

  def search_params
    params.require(:search).permit(:city_id, :topic_id, :start_from, :start_upto)
  end

end
