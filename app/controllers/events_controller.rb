class EventsController < ApplicationController
  
  def index
    @cities = City.all
    @topics = Topic.all
    events_search = EventsSearch.new(params)
    @search_params = events_search.search_params
    @events = events_search.search
  end

end
