class EventsSearch

  attr_reader :search_params

  DEFAULT_PARAMS = {
    city_id: nil,
    topic_id: nil,
    start_from: 1.month,
    start_upto: 1.month
  }

  def initialize(params)
    @search_params = {
      city_id: DEFAULT_PARAMS[:city_id],
      topic_id: DEFAULT_PARAMS[:topic_id],
      start_from: DEFAULT_PARAMS[:start_from].ago.beginning_of_day,
      start_upto: DEFAULT_PARAMS[:start_upto].since.end_of_day
    }
    if params[:search]
      @search_params[:city_id] = params[:search][:city_id]
      @search_params[:topic_id] = params[:search][:topic_id]
      @search_params[:start_from] = params[:search][:start_from]
      @search_params[:start_upto] = params[:search][:start_upto]
    end
  end

  def search
    Event.main_search(
      @search_params[:city_id],
      @search_params[:topic_id],
      @search_params[:start_from],
      @search_params[:start_upto]
    )
  end

end
