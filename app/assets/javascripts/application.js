//= require rails-ujs
//= require jquery3
//= require bootstrap
//= require_tree .

$(document).ready(function() {
  $('#save_search_params').on('click' ,function() {
    old_url = $('#search_form').attr('action');
    old_method = $('#search_form').attr('method');
    csrf = $('meta[name="csrf-token"]').attr('content');
    $('#search_form').prepend('<input type="hidden" name="authenticity_token" value='+ csrf +'>');
    $('#search_form').attr('action', '/user_searches/');
    $('#search_form').attr('method', 'post');
    $('#search_form').submit();
    return(false);
  });
});
