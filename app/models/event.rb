class Event < ApplicationRecord
  validates :name, presence: true
  validates :start_at, presence: true
  validates :end_at, presence: true
  validates :city_id, presence: true

  belongs_to :city
  has_many :events_topics
  has_many :topics, through: :events_topics

  after_commit :make_notifications, on: :create
  
  def self.main_search(city_id, topic_id, start_from, start_upto)
    res = all.includes(:city, :topics)
    res = res.where("start_at >= ?", start_from) if start_from.present?
    res = res.where("start_at <= ?", start_upto) if start_upto.present?
    res = res.where(city_id: city_id) if city_id.present?
    res = res.joins(:topics).where(topics: { id: topic_id }) if topic_id.present?
    res
  end

  protected

  def make_notifications
    UserSearch.find_by_params(start_at, city_id, topic_ids).find_each do |search|
      Notification.create(
        user_id: search.user_id,
        body: 'New interested event.',
        event_id: id,
        readed: false
      )
    end
  end

end
