class EventsTopic < ApplicationRecord
  validates :event_id, presence: true
  validates :topic_id, presence: true, uniqueness: { scope: :event_id }

  belongs_to :event
  belongs_to :topic
end
