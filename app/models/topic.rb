class Topic < ApplicationRecord
  validates :name, presence: true

  has_many :event_topics
  has_many :events, through: :event_topics
end
