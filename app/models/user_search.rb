class UserSearch < ApplicationRecord
  belongs_to :user
  belongs_to :city, optional: true
  belongs_to :topic, optional: true

  def self.find_by_params(start_at, city_id, topic_ids)
    res = all
    res = res.where("start_from <= ? AND start_upto >= ?", start_at, start_at) if start_at.present?
    res = res.where(city_id: [nil, city_id]) if city_id.present?
    topic_ids << nil
    res = res.where(topic_id: topic_ids) if topic_ids.any?
    res
  end

end
