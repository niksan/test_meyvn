class CreateUserSearch < ActiveRecord::Migration[5.2]
  def change
    create_table :user_searches do |t|
      t.integer :user_id
      t.datetime :start_from
      t.datetime :start_upto
      t.integer :city_id
      t.integer :topic_id
      t.timestamps
    end
    # default index name is too long and raise error
    add_index :user_searches, [:user_id, :start_from, :start_upto, :city_id, :topic_id], unique: true, name: 'index_user_searches_on_user_start_from_start_upto_city_topic'
  end
end
