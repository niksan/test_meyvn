class CreateEventsTopics < ActiveRecord::Migration[5.2]
  def change
    create_table :events_topics do |t|
      t.integer :event_id
      t.integer :topic_id
      t.timestamps
    end
    add_index :events_topics, [:event_id, :topic_id], unique: true
  end
end
