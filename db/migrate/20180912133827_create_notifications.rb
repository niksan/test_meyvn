class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.text :body
      t.boolean :readed, default: false
      t.integer :event_id
      t.timestamps
    end
    add_index :notifications, [:user_id, :readed]
  end
end
